#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
LANG=${LANG:-en_US}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=phpldapadmin,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PHPLDAPADMIN_LDAP_NAME="${PHPLDAPADMIN_LDAP_NAME:-KubeLDAP}"
PHPLDAPADMIN_READONLY="${PHPLDAPADMIN_READONLY:-false}"
PHPLDAPADMIN_SESSIONS_SECRET="${PHPLDAPADMIN_SESSIONS_SECRET:-changemesecretsecret}"
PHPLDAPADMIN_TREE_MODE="${PHPLDAPADMIN_TREE_MODE:-AJAXTree}"
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
TZ=${TZ:-Europe/Paris}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_STARTTLS=false
else
    OPENLDAP_STARTTLS=${OPENLDAP_STARTTLS:-false}
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=phpldapadmin.$OPENLDAP_DOMAIN
fi
if ! test "$PHPLDAPADMIN_TREE_MODE" = AJAXTree \
	-o "$PHPLDAPADMIN_TREE_MODE" = HTMLTree; then
    PHPLDAPADMIN_TREE_MODE=HTMLTree
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh

echo "Install PHP LDAP Admin Configuration"
sed -e "s|LANG|$LANG|" \
    -e "s|LDAP_BASE|$OPENLDAP_BASE|" \
    -e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
    -e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
    -e "s|LDAP_HOST|$OPENLDAP_HOST|" \
    -e "s|LDAP_NAME|$PHPLDAPADMIN_LDAP_NAME|" \
    -e "s|LDAP_PORT|$OPENLDAP_PORT|" \
    -e "s|LDAP_PROTO|$OPENLDAP_PROTO|" \
    -e "s|LDAP_STARTTLS|$OPENLDAP_STARTTLS|" \
    -e "s|SESSIONS_PASSPHRASE|$PHPLDAPADMIN_SESSIONS_SECRET|" \
    -e "s|READONLY|$PHPLDAPADMIN_READONLY|" \
    -e "s|TREE_MODE|$PHPLDAPADMIN_TREE_MODE|" \
    -e "s|TZ|$TZ|" \
    /config.sample \
    >/var/www/html/config/config.php
chmod 640 /var/www/html/config/config.php

echo "Generates PHP LDAP Admin VirtualHost Configuration"
sed -e "s|PLA_HOSTNAME|$APACHE_DOMAIN|g" \
    -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
    -e "s|SSL_TOGGLE_INCLUDE|$SSL_INCLUDE.conf|g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf

export RESET_TLS=false

. /run-apache.sh
