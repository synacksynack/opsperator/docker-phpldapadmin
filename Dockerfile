FROM opsperator/php

ARG DO_UPGRADE=
ENV LANGUAGE=en_US:en \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    PLA_VERSION=1.2.6.3

# PHP LDAP Admin image for OpenShift Origin

LABEL io.k8s.description="LDAP Directory Manager" \
      io.k8s.display-name="PHP LDAP Admin $PLA_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="phpldapadmin" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-phpldapadmin" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$PLA_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install PHP LDAP Admin dependencies" \
    && apt-get install -y --no-install-recommends ca-certificates curl \
	 locales locales-all libsasl2-2 ldap-utils \
    && if ! grep "^# ${LANG}" /etc/locale.gen >/dev/null; then \
	export LANG=en_US.UTF-8; \
    fi \
    && if ! grep "^$LANG" /etc/locale.gen >/dev/null; then \
	echo "$LANG" >>/etc/locale.gen; \
    fi \
    && touch /usr/share/locale/locale.alias \
    && locale-gen \
    && echo export LANG=${LANG} >>/etc/default/locale \
    && sed -i "s|LANG=.*|LANG=${LANG}|g" /etc/apache2/envvars \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y --no-install-recommends install libcurl4-openssl-dev \
	libfreetype6-dev libicu-dev libldap2-dev libmcrypt-dev libldb-dev \
	libmemcached-dev libpng-dev libxml2-dev libreadline-dev libonig-dev \
	libedit-dev \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
	--with-ldap-sasl \
    && docker-php-ext-install mbstring intl ldap opcache gettext readline xml \
    && docker-php-ext-enable gettext ldap intl readline xml \
    && echo "# Install PHP LDAP Admin" \
    && curl -o phpldapadmin.tar.gz -SL \
	"https://github.com/leenooks/phpLDAPadmin/archive/$PLA_VERSION.tar.gz" \
    && tar -xzf phpldapadmin.tar.gz --strip 1 -C /var/www/html \
    && echo "# Enabling PHP LDAP Admin Modules" \
    && a2enmod ldap authnz_ldap ssl rewrite \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html/config /usr/local/etc/php/conf.d \
	/var/lib/phpldapadmin; \
	do \
	    mkdir -p "$dir" 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning up" \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /etc/apache2/sites-enabled/* /etc/ldap/ldap.conf \
	phpldapadmin.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-phpldapadmin.sh"]
USER 1001
