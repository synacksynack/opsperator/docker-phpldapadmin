# k8s PHP LDAP Admin

PHP LDAP Admin image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on a PHP LDAP Admin capable OpenLAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Test with:

```
$ make syndemo
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                |    Description             | Default                                                     | Inherited From    |
| :------------------------------ | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`                | PLA VirtualHost            | `phpldapadmin.${OPENLDAP_DOMAIN}`                           | opsperator/apache |
|  `APACHE_HTTP_PORT`             | PLA Directory HTTP Port    | `8080`                                                      | opsperator/apache |
|  `LANG`                         | PLA Lang                   | `en_US`                                                     |                   |
|  `ONLY_TRUST_KUBE_CA`           | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`                | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`      | OpenLDAP Bind DN Prefix    | `cn=phpldapadmin,ou=services`                               | opsperator/apache |
|  `OPENLDAP_BIND_PW`             | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`              | OpenLDAP Domain Name       | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`                | OpenLDAP Backend Address   | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`                | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`               | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_STARTTLS`            | OpenLDAP Start TLS         | `false`                                                     |                   |
|  `PHPLDAPADMIN_LDAP_NAME`       | PLA LDAP Name              | `KubeLDAP`                                                  |                   |
|  `PHPLDAPADMIN_READONLY`        | PLA ReadOnly toggle        | `false`                                                     |                   |
|  `PHPLDAPADMIN_SESSIONS_SECRET` | PLA Sessions Secret        | `changemesecretsecret`                                      |                   |
|  `PHPLDAPADMIN_TREE_MODE`       | PLA Tree Mode              | `AJAXTree`                                                  |                   |
|  `PHP_ERRORS_LOG`               | PHP Errors Logs Output     | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`       | PHP Max Execution Time     | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`         | PHP Max File Uploads       | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`            | PHP Max Post Size          | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`      | PHP Max Upload File Size   | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`             | PHP Memory Limit           | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`                 | Apache Public Proto        | `http`                                                      | opsperator/apache |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
